# BEON Wordpress Coding Exercise

## Basic requirements

- Your preferred IDE / Code Editor.
- Your preferred browser (tested on Chrome and Firefox).
- A Wordpress local environment, including Apache or Nginx, PHP and MySQL.

## Folder structure

This repository is a Wordpress' theme incliuding an XML file to import data.
Both elements are needed to complete this challenge.
